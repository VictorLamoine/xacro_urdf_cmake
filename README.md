[![ROS](https://upload.wikimedia.org/wikipedia/commons/b/bb/Ros_logo.svg)](https://gitlab.com/VictorLamoine/ros_gitlab_ci)

This is an example package demonstrating how to generate `URDF` files from `XACRO` files at compile time rather than at run-time (inside `roslaunch` files).

http://answers.ros.org/question/254974/how-do-i-generate-urdfs-at-compile-time/

